// first part
let userNumber = Number(prompt('Enter a number'))

console.log(`The number you provided is ${userNumber}.`)

for(userNumber; userNumber > 0; userNumber--)
{
    if(userNumber <= 50)
    {
        console.log(`The current value is ${userNumber}. Terminating the loop.`)
        break
    }

    if(userNumber % 10 === 0)
    {
        console.log('The number is divisible by 10. Skipping the number..')
        continue
    }

    if(userNumber % 5 === 0)
    {
        console.log(userNumber)
    }
}
// end of first part

// second part
let sampleString = 'supercalifragilisticexpialidocious'
let sampleStringConsonants = ''

console.log(sampleString)

for(let num = 0; num < sampleString.length; num++){
    if(
        sampleString[num] == 'a' || 
        sampleString[num] == 'e' || 
        sampleString[num] == 'i' || 
        sampleString[num] == 'o' || 
        sampleString[num] == 'u'
    )
    {
        continue
    }else{
        sampleStringConsonants += sampleString[num]
    }
}

console.log(sampleStringConsonants)
// end of second part

